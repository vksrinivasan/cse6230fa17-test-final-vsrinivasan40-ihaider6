#include <stdlib.h>
#include "bfs.h"
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <inttypes.h>

typedef struct adjListnode
{
  int curr;
  struct adjListnode *next;
} adjListnode_t;

typedef struct queueNode
{
  int64_t value;
  int64_t parent;
  struct queueNode *next;
} queueNode_t;

struct _cse6230graph
{
  int *seen; // When we run bfs, know if we have seen an vertex or not
  adjListnode_t **adjacencyList; // Adjacency list for running bfs
  adjListnode_t **tailPointer; // Returns tail of adjacency list for a given node. O(1) add
  queueNode_t *queueHead; // A queue for actually running bfs
  queueNode_t *queueTail; // A queue for actually running bfs
  int actMaxIndex; // Index of the highest node + 1. How large our structures will be.
};

// Function Declarations
adjListnode_t *generateNewAdjListNode(int value);
int graph_create (size_t num_edges, const int64_t (*edges)[2], cse6230graph *graph);
void runBFS(cse6230graph graph, const int64_t key, int64_t *parent);
void graph_print(cse6230graph graph);
int graph_destroy (cse6230graph *graph);
int breadth_first_search (cse6230graph graph, int num_keys, const int64_t *keys,
                          int64_t **parents);
void runBFS(cse6230graph graph, const int64_t key, int64_t *parent);


adjListnode_t *generateNewAdjListNode(int value) {
  adjListnode_t *newNode = (adjListnode_t *)malloc(sizeof(adjListnode_t)*1);
  newNode->curr = value;
  newNode->next = NULL;
  return newNode;
}


int graph_create (size_t num_edges, const int64_t (*edges)[2], cse6230graph *graph)
{

  // Get the number of the highest valued node - This tells us how big to make
  // of our graph related structures
  int maxIndex = -1;
  int i;
  for(i = 0; i < num_edges; i++) {
    if(edges[i][0] > maxIndex)
      maxIndex = edges[i][0];
    if(edges[i][1] > maxIndex)
      maxIndex = edges[i][1];
  }
  maxIndex += 1;

  // Now actually allocate memory for the graph
  cse6230graph g = NULL;
  g = (cse6230graph)malloc(sizeof(*g));

  int *initSeen = (int *)malloc(sizeof(int)*maxIndex);
  assert(initSeen != NULL);
  memset(initSeen, 0, sizeof(int)*maxIndex);

  adjListnode_t **initAdjacencyList = (adjListnode_t **)malloc(sizeof(adjListnode_t *)*maxIndex);
  assert (initAdjacencyList != NULL);

  adjListnode_t **initTailPointer = (adjListnode_t **)malloc(sizeof(adjListnode_t *)*maxIndex);
  assert(initTailPointer != NULL);

  // Just null out these lists
  for(i = 0; i < maxIndex; i++) {
    initAdjacencyList[i] = NULL;
    initTailPointer[i] = NULL;
  }

  // Run through all the edges and populate our data structures
  for(i = 0; i < num_edges; i++) {
    int val1 = edges[i][0];
    int val2 = edges[i][1];

    // create new 'links' that will go in the respective lists
    adjListnode_t *newV1Node = generateNewAdjListNode(val1);
    adjListnode_t *newV2Node = generateNewAdjListNode(val2);

    // If there aren't edges currently for v1, or v2, add this one as the first.
    // Otherwise, make these the new ones
    if(initAdjacencyList[val1] == NULL) {
      initAdjacencyList[val1] = newV2Node;
      initTailPointer[val1] = newV2Node;
    } else {
      adjListnode_t *currTail = initTailPointer[val1];
      assert(currTail != NULL);
      currTail->next = newV2Node;
      initTailPointer[val1] = newV2Node;
    }

    if(initAdjacencyList[val2] == NULL) {
      initAdjacencyList[val2] = newV1Node;
      initTailPointer[val2] = newV1Node;
    } else {
      adjListnode_t *currTail = initTailPointer[val2];
      assert(currTail != NULL);
      currTail->next = newV1Node;
      initTailPointer[val2] = newV1Node;
    }
  }

  // Finally assign all these variables to the graph structure
  g->actMaxIndex = maxIndex;
  g->seen = initSeen;
  g->adjacencyList = initAdjacencyList;
  g->tailPointer = initTailPointer;
  g->queueHead = NULL;
  g->queueHead = NULL;

  *graph = g;

  return 0;
}

void graph_print(cse6230graph graph) {
  int numNodes = graph->actMaxIndex;
  printf("Max Num Nodes: %d\n", numNodes);

  int i;
  for(i = 0; i < numNodes; i++) {
    if(graph->adjacencyList[i] == NULL)
      continue;
    printf("%d: ", i);
    adjListnode_t *iterator = graph->adjacencyList[i];
    while(iterator != NULL) {
      printf("%d, ", iterator->curr);
      iterator = iterator->next;
    }
    printf("\t curr tail pointer: %d\n", graph->tailPointer[i]->curr);
  }
}

int graph_destroy (cse6230graph *graph)
{
  // first free seen array
  free((*graph)->seen);

  // then free adjacency lists
  int i;
  int numNodes = (*graph)->actMaxIndex;
  for(i = 0; i < numNodes; i++) {
    adjListnode_t *iterator = (*graph)->adjacencyList[i];
    adjListnode_t *curr;
    while(iterator != NULL) {
      curr = iterator;
      iterator = iterator->next;
      free(curr);
    }
  }

  // then free the adjacency list array itself
  free((*graph)->adjacencyList);

  // now free the array of tail pointers - we already freed the data at the
  // tail, so just free the structure itself
  free((*graph)->tailPointer);

  // we already freed queue nodes as we actually ran bfs, so nothing really
  // to do here

  // finally free the graph itself
  free(*graph);

  *graph = NULL;
  return 0;
}

int breadth_first_search (cse6230graph graph, int num_keys, const int64_t *keys,
                          int64_t **parents)
{
  int i;
  for(i = 0; i < num_keys; i++) {
    runBFS(graph, keys[i], parents[i]);
  }
  return 0;
}

void runBFS(cse6230graph graph, const int64_t key, int64_t *parent)
{
  // first just loop through parent and set everything to -1
  int i;
  for(i = 0; i < graph->actMaxIndex; i++) {
    parent[i] = -1;
  }

  // Initialize the queue
  graph->queueHead = (queueNode_t *)malloc(sizeof(queueNode_t)*1);
  assert(graph->queueHead != NULL);

  // Put the 'root' node at the head of the queue
  graph->queueHead->value = key;
  graph->queueHead->parent = key;
  graph->queueHead->next = NULL;
  graph->queueTail = graph->queueHead;

  // Iterate through the graph until you're done
  while(graph->queueHead != NULL) {
    // 'Remove' head of queue - We actually remove it at the end of the loop
    // but intuitively that's what's happening here
    queueNode_t *currNode = graph->queueHead;

    // Mark node as seen
    graph->seen[(int)currNode->value] = 1;

    // Mark parent in the parent array
    parent[(int)currNode->value] = currNode->parent;

    // For each of the nodes neighbors, add to the
    // queue if they haven't already been seen
    adjListnode_t *neighbors = graph->adjacencyList[(int)currNode->value];
    while(neighbors != NULL) {
      if(graph->seen[neighbors->curr] == 0) {
        graph->seen[neighbors->curr] = 1; // Just to ensure we don't pick up duplicates
        queueNode_t *newQueueNode = (queueNode_t *)malloc(sizeof(queueNode_t)*1);
        newQueueNode->value = (int64_t)neighbors->curr;
        newQueueNode->parent = currNode->value;
        newQueueNode->next = NULL;
        graph->queueTail->next = newQueueNode;
        graph->queueTail = newQueueNode;
      }
      neighbors = neighbors->next;
    }
    // actually remove current head from queue
    graph->queueHead = graph->queueHead->next;
    // Free the old head
    free(currNode);
  }
}
