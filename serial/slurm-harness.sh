#!/bin/sh
sbatch --job-name=bfs			        # Job name
sbatch --mail-type=ALL                  # Mail events (NONE, BEGIN, END, FAIL, ALL)
sbatch --mail-user=ihaider6@gatech.edu # Where to send mail  
sbatch --time=00:01:00                  # Time limit hrs:min:sec
sbatch --nodes=1                        # Just one node, but
sbatch --exclusive                      # My node alone
sbatch --output=harness_example_%j.out  # Standard output and error log

module use /nethome/tisaac3/opt/deepthought/modulefiles
module load petsc/cse6230-single
pwd; hostname; date

make
./test_bfs
date
